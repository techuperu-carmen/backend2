package com.techU.hachathon.equipo6.models;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "questions")
@JsonPropertyOrder({"id", "questionsID", "detail", "correctAnswer", "answerOptions"})
public class questionsMongo {

    @Id
    @NotNull
    private String id;
    private String questionsID;
    private String detail;
    private String correctAnswer;
    @NotNull
    private List<listAnswersOptionsM> answerOptions;


    public questionsMongo(@NotNull String id, String questionsID, String detail, List<listAnswersOptionsM> answerOptions, String correctAnswer) {
        this.id = id;
        this.questionsID = questionsID;
        this.detail = detail;
        this.answerOptions = answerOptions;
        this.correctAnswer = correctAnswer;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getQuestionsID() {
        return questionsID;
    }

    public void setQuestionsID(String questionsID) {
        this.questionsID = questionsID;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public List<listAnswersOptionsM> getAnswerOptions() {
        return answerOptions;
    }

    public void setAnswerOptions(List<listAnswersOptionsM> answerOptions) {
        answerOptions = answerOptions;
    }

    public String getCorrectAnswer() {
        return correctAnswer;
    }

    public void setCorrectAnswer(String correctAnswer) {
        this.correctAnswer = correctAnswer;
    }
}
