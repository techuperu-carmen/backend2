package com.techU.hachathon.equipo6.models;

import org.jetbrains.annotations.NotNull;

public class listAnswers {
    @NotNull
    private Integer questionID;
    @NotNull
    private Integer answerID;

    public listAnswers(Integer questionID, Integer answerID) {
        this.setQuestionID(questionID);
        this.setAnswerID(answerID);
    }

    public Integer getQuestionID() {
        return questionID;
    }

    public void setQuestionID(Integer questionID) {
        this.questionID = questionID;
    }

    public Integer getAnswerID() {
        return answerID;
    }

    public void setAnswerID(Integer answerID) {
        this.answerID = answerID;
    }


}
