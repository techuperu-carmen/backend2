package com.techU.hachathon.equipo6.models;

import org.jetbrains.annotations.NotNull;

public class listAnswersOptionsM {

    @NotNull
    private Integer answerID;
    @NotNull
    private String Opcion;

    public listAnswersOptionsM(@NotNull Integer answerID, @NotNull String Opcion) {
        this.setAnswerID(answerID);
        this.setOpcion(Opcion);
    }

    public Integer getAnswerID() {
        return answerID;
    }

    public void setAnswerID(Integer answerID) {
        this.answerID = answerID;
    }

    public String getOpcion() {
        return Opcion;
    }

    public void setOpcion(String opcion) {
        Opcion = opcion;
    }
}
