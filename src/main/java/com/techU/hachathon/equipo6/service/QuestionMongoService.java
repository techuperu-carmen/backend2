package com.techU.hachathon.equipo6.service;

import com.techU.hachathon.equipo6.models.questionsMongo;

import java.util.List;

public interface QuestionMongoService {
    List<questionsMongo> findAll();
    public questionsMongo findOne(String id);
    public questionsMongo saveQuestions(questionsMongo que);
    public void updateQuestions(questionsMongo que);
    public void deleteQuestions(String id);
}
