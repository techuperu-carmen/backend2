package com.techU.hachathon.equipo6.service;

import com.techU.hachathon.equipo6.models.answers;

import java.util.List;

public interface AnswerService {
    List<answers> findAll();
    public answers findOne(String id);
    public answers saveAnswers(answers ans);
    public void updateAnswers(answers ans);
    public void deleteAnswers(String id);
}
