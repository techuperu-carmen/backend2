package com.techU.hachathon.equipo6.service;

import com.techU.hachathon.equipo6.Repository.AnswerRepository;
import com.techU.hachathon.equipo6.Repository.QuestionMongoRepository;
import com.techU.hachathon.equipo6.models.answers;
import com.techU.hachathon.equipo6.models.questionsMongo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("QuestionMongoService")
@Transactional
public class QuestionMongoServiceImplement implements QuestionMongoService{
    private QuestionMongoRepository QuestionMongoRepository;

    @Autowired
    public QuestionMongoServiceImplement (QuestionMongoRepository questionMongoRepository) {
        this.QuestionMongoRepository =questionMongoRepository;
    }

    @Override
    public List<questionsMongo> findAll() {
        return QuestionMongoRepository.findAll();
    }

    @Override
    public questionsMongo findOne(String id) {
        return null;
    }

    @Override
    public questionsMongo saveQuestions(questionsMongo que) {
        return null;
    }

    @Override
    public void updateQuestions(questionsMongo que) {

    }

    @Override
    public void deleteQuestions(String id) {

    }
}
