package com.techU.hachathon.equipo6.controllers;

import com.techU.hachathon.equipo6.models.answers;
import com.techU.hachathon.equipo6.models.questionsMongo;
import com.techU.hachathon.equipo6.service.AnswerService;
import com.techU.hachathon.equipo6.service.QuestionMongoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("questions")
public class QuestionsMongoController {
    private final QuestionMongoService questionMongoService;
    private questionsMongo questionsMongo;

    @Autowired
    public QuestionsMongoController(QuestionMongoService questionMongoService) {
        this.questionMongoService = questionMongoService;
        this.questionsMongo = questionsMongo;
    }


    @GetMapping
    public ResponseEntity<List<questionsMongo>> questions(){
        System.out.println("Me piden la lista de preguntas");
        return ResponseEntity.ok(questionMongoService.findAll());
    }

}
