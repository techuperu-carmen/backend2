package com.techU.hachathon.equipo6.Repository;
import com.techU.hachathon.equipo6.models.questionsMongo;

import java.util.List;

public interface QuestionMongoRepository {

    List<questionsMongo> findAll();
    public questionsMongo findOne (String id);
    public questionsMongo saveQuestion(questionsMongo que);
    public void updateQuestion (questionsMongo que);
    public void deleteQuestion (questionsMongo id);
}
