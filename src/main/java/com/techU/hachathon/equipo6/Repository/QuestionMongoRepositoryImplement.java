package com.techU.hachathon.equipo6.Repository;

import com.techU.hachathon.equipo6.models.answers;
import com.techU.hachathon.equipo6.models.questionsMongo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class QuestionMongoRepositoryImplement implements QuestionMongoRepository{
    private final MongoOperations mongoOperations;

    @Autowired
    public QuestionMongoRepositoryImplement(MongoOperations mongoOperations) {
        this.mongoOperations = mongoOperations;
    }

    @Override
    public List<questionsMongo> findAll() {
        List<questionsMongo> Id = this.mongoOperations.find(new Query(), questionsMongo.class);
        return Id;
    }

    @Override
    public questionsMongo findOne(String id) {
        return null;
    }

    @Override
    public questionsMongo saveQuestion(questionsMongo que) {
        return null;
    }

    @Override
    public void updateQuestion(questionsMongo que) {

    }

    @Override
    public void deleteQuestion(questionsMongo id) {

    }
}
