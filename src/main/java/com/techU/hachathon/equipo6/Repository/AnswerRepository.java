package com.techU.hachathon.equipo6.Repository;

import com.techU.hachathon.equipo6.models.answers;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface AnswerRepository {

    List<answers> findAll();
    public answers findOne (String id);
    public answers saveAnswers(answers ans);
    public void updateAnswers (answers ans);
    public void deleteAnswer (answers id);
}
