package com.techU.hachathon.equipo6;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoDatabase;
import com.techU.hachathon.equipo6.Repository.AnswerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import com.techU.hachathon.equipo6.models.answers;

@SpringBootApplication
public class Equipo6Application {

	@Autowired
	private AnswerRepository answerRepository;

	public static void main(String[] args) {
		SpringApplication.run(Equipo6Application.class, args);
	}

//	@Override
//	public void run(String... strings) throws Exception {
//		System.out.println(AnswerRepository.findAll());
//	}

//	@Bean
//	public CommandLineRunner commandLineRunnerLog(ApplicationContext ctx)
//	{
//		return args -> {
//			System.out.println("Spring Boot funcionando");
//		};

//	}

}
